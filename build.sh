#pseudo script used to build package
export ANDROID_HOME=/opt/android/
export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$ANDROID_HOME/tools/bin/

cordova build --release
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ~/bin/android_release.keystore platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk caprel

rm -f platforms/android/app/build/outputs/apk/release/app-release.apk
/opt/android/build-tools/26.0.2/zipalign -v 4 platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk platforms/android/app/build/outputs/apk/release/app-release.apk

adb install -r platforms/android/app/build/outputs/apk/release/app-release.apk
