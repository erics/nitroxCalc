/**
 * Automatically generated file. DO NOT MODIFY
 */
package fr.cap_rel.fr.sagc_plongee.fr.nitroxCalc;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "fr.cap_rel.fr.sagc_plongee.fr.nitroxCalc";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 600;
  public static final String VERSION_NAME = "0.6.0";
}
