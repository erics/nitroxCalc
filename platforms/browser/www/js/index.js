/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    initialize: function() {
	document.getElementById("valNitrox").textContent=32 + " %";
	document.getElementById("valppo2").textContent=1.4;

	var localStorage = window.sessionStorage;
	
	localStorage.setItem("ppo2", 1.4);
	localStorage.setItem("nitrox", 32);
	nitroxCalc(0);
    }
};

app.initialize();

function alertDismissed() {
    // do something
}

function nitroxCalc(val) {
    var localStorage = window.sessionStorage;

    var ppo2 = parseFloat(localStorage.getItem("ppo2"));
    var nitrox = parseInt(localStorage.getItem("nitrox")) + val;

    localStorage.setItem("nitrox", nitrox);

    document.getElementById("valNitrox").textContent = nitrox + " %";

    var resA = eval(ppo2/(nitrox/100));
    var resB = Math.floor(eval((resA-1)*10));
    document.getElementById("repProf").textContent = "" + resB + " mètres";
    document.getElementById("repA").textContent = ppo2.toFixed(2) + " ÷ " + Math.floor(nitrox)/100 + " = " + resA.toFixed(2) + " (bar)";
    document.getElementById("repB").textContent = "(" + resA.toFixed(2) + " - 1) × 10 = " + resB  + " mètres";
}

function nitroxCalcPPo2(val) {
    var localStorage = window.sessionStorage;

    var ppo2 = parseFloat(localStorage.getItem("ppo2")) + val;
    var nitrox = parseInt(localStorage.getItem("nitrox"));

    localStorage.setItem("ppo2", ppo2);

    document.getElementById("valppo2").textContent = ppo2.toFixed(1);

    var resA = eval(ppo2/(nitrox/100));
    var resB = Math.floor(eval((resA-1)*10));
    document.getElementById("repProf").textContent = "" + resB + " mètres";
    document.getElementById("repA").textContent = ppo2.toFixed(2) + " ÷ " + Math.floor(nitrox)/100 + " = " + resA.toFixed(2) + " (bar)";
    document.getElementById("repB").textContent = "(" + resA.toFixed(2) + " - 1) × 10 = " + resB  + " mètres";
}
