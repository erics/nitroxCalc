nitroxCalc is a fast and easy app to compute maximum depth for diving with nitrox

[<img src="https://devimages-cdn.apple.com/app-store/marketing/guidelines/images/badge-example-preferred.png"
      alt="Get it on Apple Store"
      height="54">](https://itunes.apple.com/fr/app/nitroxcalc/id1408060704/)
[<img src="https://f-droid.org/badge/get-it-on.png"
      alt="Get it on F-Droid"
      height="80">](https://f-droid.org/packages/fr.cap_rel.fr.sagc_plongee.fr.nitroxCalc/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
      alt="Get it on Google Play"
      height="80">](https://play.google.com/store/apps/details?id=fr.cap_rel.fr.sagc_plongee.fr.nitroxCalc)

Code is GNU/GPL v3 - Éric Seigne <eric.seigne@videosub.fr>

Full setup (from a debian strech)
  apt-get install openjdk-8-jre openjdk-8-jdk android-sdk android-sdk-platform-23 nodejs-legacy nodejs libgradle-android-plugin-java
  npm install -g cordova #@6.5.0 (due to cordova 7.0 bug cf https://github.com/phonegap/phonegap-plugin-push/issues/1724)

  From https://wiki.debian.org/AndroidTools/IntroBuildingApps
    git clone https://github.com/mherod/android-sdk-licenses.git
    sudo cp -a android-sdk-licenses/. /usr/lib/android-sdk/licenses

Plugins:
	cordova plugin add cordova-plugin-dialogs 
	cordova plugin add cordova-plugin-whitelist

Build:
	cordova build android
	